package net.poundex.sentinel.emissary.vox.client

import feign.Headers
import feign.RequestLine
import net.poundex.sentinel.emissary.vox.SpeechSynthRequest
import net.poundex.sentinel.emissary.vox.SpeechSynthResponse

interface SpeechSynthClient
{
	@RequestLine("POST /speech/synth")
	@Headers(["Content-type: application/json", "Accept: application/json"])
	SpeechSynthResponse synthSpeech(SpeechSynthRequest speechSynthRequest)
}
