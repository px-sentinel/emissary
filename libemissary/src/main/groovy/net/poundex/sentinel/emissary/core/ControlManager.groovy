package net.poundex.sentinel.emissary.core

import net.poundex.sentinel.emissary.scribe.client.ProcessSpeechResult


class ControlManager implements ControlListener
{
	private final List<ControlListener> listeners = []

	void addControlListener(ControlListener controlListener)
	{
		listeners << controlListener
	}

	@Override
	void onListeningStarted()
	{
		listeners*.onListeningStarted()
	}

	@Override
	void onHotwordRecognised()
	{
		listeners*.onHotwordRecognised()
	}

	@Override
	void onSpeechProcessed(ProcessSpeechResult processSpeechResult)
	{
		listeners*.onSpeechProcessed(processSpeechResult)
	}

	@Override
	void onSpeechReceived(byte[] bytes)
	{
		listeners*.onSpeechReceived(bytes)
	}
}
