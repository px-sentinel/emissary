package net.poundex.sentinel.emissary.fathom

interface  IntentRegistry
{
	void register(Intent intent, String intentName)
}
