package net.poundex.sentinel.emissary.core

interface RecognitionProvider
{
	void waitForHotword()

	byte[] getSpeech()
	
	void stop()
}
