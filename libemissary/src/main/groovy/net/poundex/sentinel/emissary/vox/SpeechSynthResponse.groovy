package net.poundex.sentinel.emissary.vox

import groovy.transform.Canonical

@Canonical
class SpeechSynthResponse
{
	String speechData
}
