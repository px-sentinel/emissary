package net.poundex.sentinel.emissary.fathom.client

import feign.Headers
import feign.RequestLine
import net.poundex.sentinel.emissary.fathom.Query
import net.poundex.sentinel.emissary.fathom.ResponsePackage 

interface QueryClient
{
	@RequestLine("POST /query")
	@Headers(["Content-type: application/json", "Accept: application/json"])
	ResponsePackage query(Query query)
}
