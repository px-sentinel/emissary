package net.poundex.sentinel.emissary.vox

import java.nio.file.Path

interface SpeechPostProcessor
{
	Path process(Path speechFile)
}
