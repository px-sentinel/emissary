package net.poundex.sentinel.emissary.ctx

import feign.Feign
import feign.jackson.JacksonDecoder
import feign.jackson.JacksonEncoder
import net.poundex.sentinel.emissary.core.ControlLoop
import net.poundex.sentinel.emissary.core.ControlManager
import net.poundex.sentinel.emissary.core.RecognitionProvider
import net.poundex.sentinel.emissary.fathom.client.QueryClient
import net.poundex.sentinel.emissary.scribe.client.ScribeClient
import net.poundex.sentinel.emissary.vox.client.SpeechSynthClient
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackages = "net.poundex.sentinel.emissary")
public class EmissaryConfiguration
{
	@Bean
	ControlLoop controlLoop(RecognitionProvider recognitionProvider, ScribeClient scribeClient, ControlManager controlManager)
	{
		return new ControlLoop(recognitionProvider, scribeClient, controlManager);
	}

	@Bean
	ScribeClient scribeClient()
	{
		return Feign
				.builder()
				.encoder(new JacksonEncoder())
				.decoder(new JacksonDecoder())
				.target(ScribeClient.class, "http://localhost:10199");
	}

	@Bean
	ControlManager controlManager()
	{
		return new ControlManager();
	}

	@Bean
	QueryClient queryClient()
	{
		return Feign
				.builder()
				.encoder(new JacksonEncoder())
				.decoder(new JacksonDecoder())
				.target(QueryClient.class, "http://localhost:10199");
	}

	@Bean
	SpeechSynthClient speechSynthClient()
	{
		return Feign
				.builder()
				.encoder(new JacksonEncoder())
				.decoder(new JacksonDecoder())
				.target(SpeechSynthClient.class, "http://localhost:10199");
	}
}
