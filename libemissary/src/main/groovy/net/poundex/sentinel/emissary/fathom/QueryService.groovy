package net.poundex.sentinel.emissary.fathom

interface QueryService
{
	QueryResult query(Query query)
}
