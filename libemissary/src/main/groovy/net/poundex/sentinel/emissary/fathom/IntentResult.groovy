package net.poundex.sentinel.emissary.fathom

abstract class IntentResult
{
	boolean isSuccess()
	{
		return true
	}

	abstract List<Response> respond()
}
