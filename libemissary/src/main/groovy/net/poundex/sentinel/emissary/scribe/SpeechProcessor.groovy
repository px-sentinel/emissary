package net.poundex.sentinel.emissary.scribe

interface SpeechProcessor
{
	String processSpeech(byte[] wave)
}
