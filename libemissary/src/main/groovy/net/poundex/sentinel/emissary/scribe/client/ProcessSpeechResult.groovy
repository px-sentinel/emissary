package net.poundex.sentinel.emissary.scribe.client

import groovy.transform.Immutable

@Immutable
class ProcessSpeechResult
{
	String speech
}
