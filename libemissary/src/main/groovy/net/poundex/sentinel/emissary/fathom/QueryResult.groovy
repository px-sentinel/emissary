package net.poundex.sentinel.emissary.fathom

import groovy.transform.Immutable

@Immutable
class QueryResult
{
	String text
	Map<String, String> parameters
}
