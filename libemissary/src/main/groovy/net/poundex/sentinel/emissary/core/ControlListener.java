package net.poundex.sentinel.emissary.core;

import net.poundex.sentinel.emissary.scribe.client.ProcessSpeechResult;

public interface ControlListener
{
	default void onListeningStarted() { }

	default void onHotwordRecognised() { }

	default void onSpeechProcessed(ProcessSpeechResult processSpeechResult) { }

	default void onSpeechReceived(byte[] bytes) { }
}
