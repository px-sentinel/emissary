package net.poundex.sentinel.emissary.fathom

import groovy.transform.Canonical

@Canonical
class ResponsePackage
{
	List<ResponseCommand> responseCommands = []
}
