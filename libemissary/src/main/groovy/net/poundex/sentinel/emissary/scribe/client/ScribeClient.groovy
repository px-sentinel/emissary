package net.poundex.sentinel.emissary.scribe.client


import feign.Headers
import feign.RequestLine

interface ScribeClient
{
	@RequestLine("POST /processSpeech/process")
	@Headers("Content-type: application/json")
	ProcessSpeechResult processSpeech(ProcessSpeechCommand processSpeechCommand)
}
