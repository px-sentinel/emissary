package net.poundex.sentinel.emissary.fathom

interface QueryProcessor
{
	QueryResult processQuery(Query query)
}
