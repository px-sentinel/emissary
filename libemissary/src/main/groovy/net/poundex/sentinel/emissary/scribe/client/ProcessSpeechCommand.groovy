package net.poundex.sentinel.emissary.scribe.client

import groovy.transform.Canonical

@Canonical
class ProcessSpeechCommand
{
	String wave;
}
