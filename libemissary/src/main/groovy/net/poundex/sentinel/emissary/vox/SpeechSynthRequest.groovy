package net.poundex.sentinel.emissary.vox

import groovy.transform.Canonical

@Canonical
class SpeechSynthRequest
{
	String speech
}
