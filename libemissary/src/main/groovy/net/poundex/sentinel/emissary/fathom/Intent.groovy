package net.poundex.sentinel.emissary.fathom

interface Intent
{
	void registerIn(IntentRegistry intentRegistry)
	IntentResult run(QueryResult queryResult)
}
