package net.poundex.sentinel.emissary.fathom

import groovy.transform.Canonical

@Canonical
class Query
{
	String query
}
