package net.poundex.sentinel.emissary.vox

import java.nio.file.Path

interface SpeechSynthesizer
{
	Path synthSpeech(SpeechSynthRequest speechSynthRequest)
}
