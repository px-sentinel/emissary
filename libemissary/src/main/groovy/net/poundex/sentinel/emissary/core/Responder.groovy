package net.poundex.sentinel.emissary.core

import net.poundex.sentinel.emissary.fathom.ResponsePackage


interface Responder
{
	void respond(ResponsePackage responsePackage)
}
